 module uart_loopback_top(
     input           sys_clk,            //External 50M clock
     input           sys_rst_n,
     input           uart_rxd,
     output          uart_txd
);
 
 parameter  CLK_FREQ = 50000000;    //system clock frequency
 parameter  UART_BPS = 115200;      //serial port baud rate
     
 wire       uart_recv_done;             //UART reception completed
 wire       [7:0] uart_recv_data;       //UART receive data
 wire       uart_send_en;                //UART send enable
 wire       [7:0] uart_send_data;   //UART send data
 wire       uart_tx_busy;               //UART sends busy status flag

//Serial receiving module  
uart_recv #( 
    .CLK_FREQ       (CLK_FREQ),         //system clock frequency
    .UART_BPS       (UART_BPS))         //serial port baud rate
u_uart_recv( 
    .sys_clk        (sys_clk), 
    .sys_rst_n      (sys_rst_n),
    
    .uart_rxd       (uart_rxd),
    .uart_done      (uart_recv_done),
    .uart_data      (uart_recv_data)
);
 
//Serial sending module    
 uart_send #( 
     .CLK_FREQ       (CLK_FREQ),         //system clock frequency
     .UART_BPS       (UART_BPS))         //serial port baud rate
 u_uart_send( 
     .sys_clk        (sys_clk),
     .sys_rst_n      (sys_rst_n),
      
     .uart_en        (uart_send_en),
     .uart_din       (uart_send_data),
     .uart_tx_busy   (uart_tx_busy),
     .uart_txd       (uart_txd)
     );
     
 //Serial loopback module
 uart_loop u_uart_loop(
    .sys_clk        (sys_clk), 
    .sys_rst_n      (sys_rst_n), 
    
    .recv_done      (uart_recv_done),   //Receive a frame of data completion flag signal
    .recv_data      (uart_recv_data),
    
    .tx_busy        (uart_tx_busy),     //Send busy status flag   
    .send_en        (uart_send_en),
    .send_data      (uart_send_data)
);
     
endmodule
